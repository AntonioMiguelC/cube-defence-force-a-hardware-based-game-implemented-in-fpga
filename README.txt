This is a simple "Catch the ball" game implemented in VHDL. This was a project for The University of Auckland course COMPSYS 305.It was completed in the first semester of 2015.

Acknowledgements go to my teammate Kingsly Delpachitra.

Prerequisites:
Altera FPGA board (Cyclone II or equivalent)
Mouse (PS2)
Quartus II (version 13 or later)

Optional:
PS2 splitter
PS2 Keyboard

Please see the included report for more details.

-Antonio




