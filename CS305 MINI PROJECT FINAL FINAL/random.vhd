LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

Entity random is

	port (	clk				:	in std_logic;
				output			:	out std_logic_vector(3 downto 0)
			);
			
end entity;

Architecture arch of random is

Begin

	process(clk)
	variable rand_temp : std_logic_vector (3 downto 0):= "1010";
	variable temp : std_logic := '0';	
	begin
	
		if rising_edge(clk) then
			temp := rand_temp(3) xor rand_temp(2);
			rand_temp(3 downto 1) := rand_temp(2 downto 0);
			rand_temp(0) := temp;
		end if;
		output <= rand_temp;
			
	end process;
end arch;