-- A Mealy machine has outputs that depend on both the state and
-- the inputs.	When the inputs change, the outputs are updated
-- immediately, without waiting for a clock edge.  The outputs
-- can be written more than once per state or per clock cycle.
-- DISCLAIMER: TEMPLATE TAKEN FROM ALTERA WEBSITE

library ieee;
use ieee.std_logic_1164.all;

entity Control_Unit is

	port
	(
		clk		 		: in	std_logic;
		start				: in	std_logic;
		timer_done		: in	std_logic;
		lives_zero		: in	std_logic;
		SW0			 	: in	std_logic;
		SW1				: in	std_logic;
		reset	 			: in	std_logic;
		max_score		: in	std_logic;
		SW2				: in	std_logic;
		key_in			: in 	std_logic_vector(7 downto 0);
		new_key			: in	std_logic;
		game_sel 		: out	std_logic;
		reset_game		: out std_logic;
		train_sel		: out std_logic;
		mode_sel			: out std_logic;
		game_done		: out std_logic;
		has_won			: out std_logic;
		disp_score		: out std_logic;
		mult_on			: out std_logic
	);
	
end entity;

architecture rtl of control_unit is

	-- Build an enumerated type for the state machine
	type state_type is (WAITING, MODESELECT, TRAINING, GAME, LOSE, WIN, SCORESCREEN, MULT);
	
	-- Register to hold the current state
	signal state : state_type;

begin
	process (clk, reset, start)
	begin
		if reset = '0' then
			state <= WAITING;
		elsif (rising_edge(clk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
			
				when WAITING=>
					if start = '1' or key_in = "00101001" then
						state <= MODESELECT;
					else
						state <= WAITING;
					end if;
					
				when MODESELECT =>
					if (SW0 = '1' AND SW1 = '0') AND (start = '1' or (key_in = "00101001" and new_key = '1')) then
						state <= GAME;
					elsif (SW0 = '0' AND SW1 = '1') AND (start = '1' or (key_in = "00101001" and new_key = '1')) then
						state <= TRAINING;
					elsif ((SW0 = '0' AND SW1 = '0') AND (SW2 = '1')) AND (start = '1' or (key_in = "00101001" and new_key = '1')) then
						state <= MULT;
					else
						state <= MODESELECT;
					end if;
					
				when TRAINING =>
					if max_score = '1' then
						state <= SCORESCREEN;
					else
						state <= TRAINING;
					end if;
					
				when GAME=>
					if lives_zero = '1' then
						state <= LOSE;
					elsif timer_done = '1' then
						state <= WIN;
					else
						state <= GAME;
					end if;
					
				when LOSE =>
					if start = '1' then
						state <= WAITING;
					else
						state <= LOSE;
					end if;
					
				when WIN =>
					if start = '1' then
						state <= WAITING;
					else
						state <= WIN;
					end if;
				
				when SCORESCREEN =>
					if start = '1' then
						state <= WAITING;
					else
						state <= SCORESCREEN;
					end if;
				when MULT =>
					if max_score = '1' then
						state <= SCORESCREEN;
					else
						state <= MULT;
					end if;
					
			end case;
			
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state)
	begin
		case state is
		
			when WAITING=>
				game_sel <= '0';
				train_sel <= '0';
				reset_game <= '1';
				mode_sel <= '0';
				game_done <= '0';
				has_won <= '0';
				disp_score <= '0';
				mult_on <= '0';
				
			when MODESELECT =>
				game_sel <= '0';
				train_sel <= '0';
				reset_game <= '1';
				mode_sel <= '1';
				game_done <= '0';
				has_won <= '0';
				disp_score <= '0';
				mult_on <= '0';
				
			when TRAINING =>
				game_sel <= '1';
				train_sel <= '1';
				reset_game <= '0';
				mode_sel <= '0';
				game_done <= '0';
				has_won <= '0';
				disp_score <= '0';
				mult_on <= '0';
				
			when GAME=>
				game_sel <= '1';
				train_sel <= '0';
				reset_game <= '0';
				mode_sel <= '0';
				game_done <= '0';
				has_won <= '0';
				disp_score <= '0';
				mult_on <= '0';
				
			when LOSE =>
				game_sel <= '0';
				train_sel <= '0';
				reset_game <= '0';
				mode_sel <= '0';
				game_done <= '1';
				has_won <= '0';
				disp_score <= '1';
				mult_on <= '0';
			
			when WIN =>
				game_sel <= '0';
				train_sel <= '0';
				reset_game <= '0';
				mode_sel <= '0';
				game_done <= '1';
				has_won <= '1';
				disp_score <= '1';
				mult_on <= '0';
			
			when SCORESCREEN =>
				game_sel <= '0';
				train_sel <= '0';
				reset_game <= '0';
				mode_sel <= '0';
				game_done <= '1';
				has_won <= '1';
				disp_score <= '1';
				mult_on <= '0';
			
			when MULT =>
				game_sel <= '1';
				train_sel <= '1';
				reset_game <= '0';
				mode_sel <= '0';
				game_done <= '0';
				has_won <= '0';
				disp_score <= '0';
				mult_on <= '1';
			
			
		end case;
	end process;
	
end rtl;
