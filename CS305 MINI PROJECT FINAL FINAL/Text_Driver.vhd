LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

Entity Text_driver is

port(output							               : out std_logic_vector(5 downto 0);
	  lose_game						               : out std_logic;
	  up_ball_speed									: out STD_LOGIC;
     counter_in, Down_counter, update_life 	: in std_logic;
     pixel_row 					               : in std_logic_vector(9 downto 0);
	  pixel_column					               : in std_logic_vector(9 downto 0);
	  pause											   : in std_logic;
	  P_clk                                   : in std_logic;
	  reset												: in std_logic;
	  train_sel											: in std_logic;
	  count_zero										: out std_logic;
	  rst_ball_speed									: out std_logic;
	  score_99											: out std_logic
	 									   
	  );
	  
End Text_driver;

Architecture Behaviour of Text_driver is
SIGNAL temp_ONES: STD_LOGIC_VECTOR(3 DOWNTO 0); 
SIGNAL temp_tens: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL lives	 :	integer;
SIGNAL TMP_ONES : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL TMP_TENS : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL lvl_ones : STD_LOGIC_VECTOR(3 DOWNTO 0):= "0001";
SIGNAL lvl_tens : STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";
SIGNAL time_P   : STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";


Begin

Text_Display: Process(pause, pixel_column, pixel_row,temp_ones, temp_tens, lives)

BEGIN

--------------------------------------------------------
--DISPLAY SCORE
--------------------------------------------------------


  IF (pixel_column(9 downto 4) = "011111") AND
 	 	
 	  (pixel_row(9 downto 4)  = "000001") then
	  
	  		output <= "010011"; --Display the letter 'S'
		
	ELSIF (pixel_column(9 downto 4) = "100000") AND
 			 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "000011"; --Display the letter 'C'
			 
	ELSIF (pixel_column(9 downto 4) = "100001") AND
 			 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "001111"; --Display the letter 'O'
			 
	ELSIF (pixel_column(9 downto 4) = "100010") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "010010"; --Display the letter 'R'
			 
	ELSIF (pixel_column(9 downto 4) = "100011") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "000101"; --Display the letter 'E'
		 
	ELSIF (pixel_column(9 downto 4) = "100100") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "111111"; --Display Colon
			 
	ELSIF (pixel_column(9 downto 4) = "100101") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    case temp_tens is

			when "0000" => output <= "110000" ;
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;
			 	
	ELSIF (pixel_column(9 downto 4) = "100110") AND
	
 		   (pixel_row   (9 downto 4) = "000001") then
		
	case temp_ones is

	when "0000" => output <= "110000" ;
	when "0001" => output <= "110001";
	when "0010" => output <= "110010";
	when "0011" => output <= "110011";
	when "0100" => output <= "110100";
	when "0101" => output <= "110101";
	when "0110" => output <= "110110";
	when "0111" => output <= "110111";
	when "1000" => output <= "111000";
	when "1001" => output <= "111001";
	when others => output <= "100000";
	END CASE;
	
----------------------------------------------------------
--DISPLAY LIVES
----------------------------------------------------------	
	
	ELSIF (pixel_column(9 downto 4) = "011111") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "001100"; --Display 'L'
			
			 
	ELSIF (pixel_column(9 downto 4) = "100000") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
				
			
			 output <= "001001"; --Display 'I'
			 
	ELSIF (pixel_column(9 downto 4) = "100001") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
							
			 output <= "010110"; --Display 'V'
			 
	ELSIF (pixel_column(9 downto 4) = "100010") AND
	
 		   (pixel_row   (9 downto 4) = "000010") then
	
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "100011") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
					
			 output <= "010011"; --Display 'S'
			 
	ELSIF (pixel_column(9 downto 4) = "100100") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
					
			 output <= "111111"; --Display Colon	

  ELSIF (pixel_column(9 downto 4) = "100101") AND
	
 		  (pixel_row(9 downto 4)  = "000010") then
	
			 output <= "110000"; --Display '0'	

 ELSIF (pixel_column(9 downto 4) = "100110") AND
	
 		  (pixel_row(9 downto 4)  = "000010") then

			 case lives is

	when 0 => output <= "110000" ;
	when 1 => output <= "110001";
	when 2 => output <= "110010";
	when 3 => output <= "110011";
	when 4 => output <= "110100";
	when 5 => output <= "110101";
	when 6 => output <= "110110";
	when 7 => output <= "110111";
	when 8 => output <= "111000";
	when 9 => output <= "111001";
	when others => output <= "100000";
	END CASE;	
			 
--------------------------------------------------------------
--DISPLAY LEVEL
--------------------------------------------------------------
			 
	ELSIF (pixel_column(9 downto 4) = "000001") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "001100"; --Display 'L'
			 
	ELSIF (pixel_column(9 downto 4) = "000010") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "000011") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "010110"; --Display 'V'
			 
	ELSIF (pixel_column(9 downto 4) = "000100") AND
	
 		   (pixel_row   (9 downto 4) = "000001") then
			
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "000101") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "001100"; --Display 'L'
			 
	ELSIF (pixel_column(9 downto 4) = "000110") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "111111"; --Display Colon	

  ELSIF (pixel_column(9 downto 4) = "000111") AND
	
 		  (pixel_row(9 downto 4)  = "000001") then
			
			 case lvl_tens is

			when "0000" => output <= "110000";
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;		

 ELSIF (pixel_column(9 downto 4) = "001000") AND
	
 		  (pixel_row(9 downto 4)  = "000001") then
			
			  case lvl_ones is

			when "0000" => output <= "110000";
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;			 

-------------------------------------------------------
--DISPLAY TIMER
-------------------------------------------------------
	ELSIF (pixel_column(9 downto 4) = "000001") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "010100"; --Display 'T'
			 
	ELSIF (pixel_column(9 downto 4) = "000010") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "001001"; --Display 'I'
			 
	ELSIF (pixel_column(9 downto 4) = "000011") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "001101"; --Display 'M'
			 
	ELSIF (pixel_column(9 downto 4) = "000100") AND
	
 		   (pixel_row   (9 downto 4) = "000010") then
			
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "000101") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "010010"; --Display 'R'
			 
	ELSIF (pixel_column(9 downto 4) = "000110") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "111111"; --Display Colon	

  ELSIF (pixel_column(9 downto 4) = "000111") AND
	
 		  (pixel_row(9 downto 4)  = "000010") then
			
		  	  
 		    case TMP_TENS is

			when "0000" => output <= "110000";
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;	

 ELSIF (pixel_column(9 downto 4) = "001000") AND
	
 		 (pixel_row(9 downto 4)  = "000010") then
			
			 
 		    case TMP_ONES is

			when "0000" => output <= "110000";
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;
--------------------------------------------------
--DISPLAY PAUSE			
-------------------------------------------------
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(17,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
				
				(pause = '1') THEN
				
			--output <= "010000"; --Display 'P'
								
			   CASE time_P is
			
					when "0000" => output <= "100000";--
					when "0001" => output <= "100000";
					when "0010" => output <= "100000";
					when "0011" => output <= "010000";
					when "0100" => output <= "010000";
					when "0101" => output <= "010000";
					when "0110" => output <= "010000";
					when "0111" => output <= "010000";
					when "1000" => output <= "010000";
					when "1001" => output <= "010000";
					when "1010" => output <= "100000";
					when "1011" => output <= "100000";
					when "1100" => output <= "100000";
					when "1101" => output <= "100000";
					when others => output <= "100000";
					
				END CASE;
						 
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(18,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
				
		      (pause = '1') THEN
				
			   --output <= "000001"; --Display 'A'
				
				  CASE time_P is
				
				   when "0000" => output <= "100000";
					when "0001" => output <= "100000";
					when "0010" => output <= "000001";--
					when "0011" => output <= "000001";
					when "0100" => output <= "000001";
					when "0101" => output <= "000001";
					when "0110" => output <= "000001";
					when "0111" => output <= "000001";
					when "1000" => output <= "000001";
					when "1001" => output <= "100000";
					when "1010" => output <= "100000";
					when "1011" => output <= "100000";
					when "1100" => output <= "100000";
					when "1101" => output <= "100000";
					when others => output <= "100000";
					
				 END CASE;
						 
	   ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(19,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
							
				(pause = '1') THEN
				
			  --output <= "010101"; --Display 'U'
				 
				 CASE time_P is
				
				   when "0000" => output <= "100000";
					when "0001" => output <= "010101";
					when "0010" => output <= "010101";--
					when "0011" => output <= "010101";
					when "0100" => output <= "010101";
					when "0101" => output <= "010101";
					when "0110" => output <= "010101";
					when "0111" => output <= "010101";
					when "1000" => output <= "100000";
					when "1001" => output <= "100000";
					when "1010" => output <= "100000";
					when "1011" => output <= "100000";
					when "1100" => output <= "100000";
					when "1101" => output <= "100000";
					when others => output <= "100000";
					
				 END CASE;
					 
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(20,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
							
				(pause = '1') THEN
				
			  --output <= "010011"; --Display 'S'
				
				 CASE time_P is
				
				   when "0000" => output <= "010011";--
					when "0001" => output <= "010011";
					when "0010" => output <= "010011";
					when "0011" => output <= "010011";
					when "0100" => output <= "010011";
					when "0101" => output <= "010011";
					when "0110" => output <= "010011";
					when "0111" => output <= "100000";
					when "1000" => output <= "100000";
					when "1001" => output <= "100000";
					when "1010" => output <= "100000";
					when "1011" => output <= "100000";
					when "1100" => output <= "100000";
					when "1101" => output <= "100000";
					when others => output <= "100000";
					
				 END CASE;
						 
	   ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(21,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
							
				(pause = '1') THEN
				
			  --output <= "000101"; --Display 'E'
			
				 CASE time_P is
				
				   when "0000" => output <= "100000";
					when "0001" => output <= "000101";
					when "0010" => output <= "000101";
					when "0011" => output <= "000101";
					when "0100" => output <= "000101";--
					when "0101" => output <= "000101";
					when "0110" => output <= "000101";
					when "0111" => output <= "000101";
					when "1000" => output <= "100000";
					when "1001" => output <= "100000";
					when "1010" => output <= "100000";
					when "1011" => output <= "100000";
					when "1100" => output <= "100000";
					when "1101" => output <= "100000";
					when others => output <= "100000";
					
				 END CASE;
					
				
			 
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(22,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
				
			  --output <= "000100"; --Display 'D'
			
				 CASE time_P is
				
				   when "0000" => output <= "100000";
					when "0001" => output <= "100000";
					when "0010" => output <= "000100";
					when "0011" => output <= "000100";
					when "0100" => output <= "000100";
					when "0101" => output <= "000100";--
					when "0110" => output <= "000100";
					when "0111" => output <= "000100";
					when "1000" => output <= "000100";
					when "1001" => output <= "100000";
					when "1010" => output <= "100000";
					when "1011" => output <= "100000";
					when "1100" => output <= "100000";
					when "1101" => output <= "100000";
					when others => output <= "100000";
					
				 END CASE;
					
				
				
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(23,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
				
				(pause = '1') THEN
						 
			  --output <= "100001"; --Display '!'	
			
				 CASE time_P is
				
				   when "0000" => output <= "100000";
					when "0001" => output <= "100000";
					when "0010" => output <= "100000";
					when "0011" => output <= "100001";
					when "0100" => output <= "100001";
					when "0101" => output <= "100001";
					when "0110" => output <= "100001";--
					when "0111" => output <= "100001";
					when "1000" => output <= "100001";
					when "1001" => output <= "100001";
					when "1010" => output <= "100000";
					when "1011" => output <= "100000";
					when "1100" => output <= "100000";
					when "1101" => output <= "100000";
					when others => output <= "100000";
					
				 END CASE;
		
	
		ELSE
				output <= "100000"; --Display Spaces
	END IF;	 

END process Text_Display;

------------------------------------------------------
------------------------------------------------------

--CONV_STD_LOGIC_VECTOR(640,10)

Score_Count: Process(counter_in)

	Begin
			
			if reset = '1' then
				temp_ones <= "0000";
				temp_tens <= "0000";
				lvl_ones <= "0001";
				lvl_tens <= "0000";
				up_ball_speed <= '0';
				rst_ball_speed <= '1';
				score_99 <= '0';
			else
	
				if (counter_in = '1') then
			  
					if (temp_ones = "1001") and (temp_tens /= "1001") then
			  
						temp_ones <= "0000";
						temp_tens <= temp_tens + 1;
						up_ball_speed <= '1';
						rst_ball_speed <= '0';
						score_99 <= '0';
						if lvl_ones  = "1001" then
							lvl_ones <= "0000";
							lvl_tens <= lvl_tens + "0001";
						else
							lvl_ones <= lvl_ones + "0001";
						end if;
					
					elsif (temp_ones = "1001") and (temp_tens = "1001") then
						temp_ones <= "0000";
						temp_tens <= "0000";
						up_ball_speed <= '1';
						rst_ball_speed <= '0';
						score_99 <= '1';
						if lvl_ones  = "1001" then
							lvl_ones <= "0000";
							lvl_tens <= lvl_tens + "0001";
						else
							lvl_ones <= lvl_ones + "0001";
						end if;
					else
			        temp_ones <= temp_ones + 1;
					  up_ball_speed <= '0';
					  rst_ball_speed <= '0';
					  score_99 <= '0';
					  
					end if;
				end if;
			end if;
		end process;
		
LIVES_COUNT: Process(update_life)

begin

if reset = '1' or train_sel = '1' then
	lives <= 9;
	lose_game <= '0';
else

	if (update_life = '1')  then
		if(lives = 1) then
			lose_game <= '1';
		else 
			lives <= lives - 1;
			lose_game <= '0';
		end if;
	end if;
end if;

end process;

Time_Count: Process(Down_counter)

	Begin
		
		if reset = '1' then
			TMP_TENS <= "0110";
			TMP_ONES <= "0000";
			count_zero <= '0';
		elsif train_sel = '1' then
			TMP_TENS <= "0000";
			TMP_ONES <= "0000";
			count_zero <= '0';
		else
			if (Down_counter = '1') then
			  
			  if (TMP_ONES = "0000") and (TMP_TENS /= "0000") then
			  
			      TMP_ONES <= "1001";
					TMP_TENS <= TMP_TENS - "0001";
					count_zero <= '0';
					
			  elsif (TMP_ONES = "0000") and (TMP_TENS = "0000") then
						count_zero <= '1';

						TMP_TENS <= "0011";
			  else
			        TMP_ONES <= TMP_ONES - 1;
					  count_zero <= '0';
					  
				end if;
			end if;
		end if;
		end process;
		
Pause_Count: Process(P_clk)

  	Begin
			if (P_clk = '1') then
			  
					if (time_P = "1111") then
			  
			          time_P <= "0000"; 
		
					else
					
					    time_P <= time_P + 1;
										  
				end if;
			end if;
end process;

End Behaviour;
