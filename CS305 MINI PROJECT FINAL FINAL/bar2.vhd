LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;
LIBRARY lpm;
USE lpm.lpm_components.ALL;

ENTITY bar2 IS
Generic(ADDR_WIDTH: integer := 12; DATA_WIDTH: integer := 1);

PORT(	  SIGNAL Red,Green,Blue 								: OUT std_logic;
		  SIGNAL X_Pos, Y_Pos, bar_height, bar_width		: OUT std_logic_vector(9 downto 0);
		  SIGNAL key_in											: IN std_logic_vector (7 downto 0);
		  SIGNAL pixel_row, pixel_column						: IN std_logic_vector (9 downto 0);
        SIGNAL Vert_sync, pause								: IN std_logic;
		  SIGNAL new_key											: IN std_logic);		
END bar2;

ARCHITECTURE arch of bar2 is

SIGNAL bar_on						:	STD_LOGIC;
SIGNAL barheight, barwidth		:	STD_LOGIC_VECTOR (9 downto 0);
SIGNAL bar_X_pos					:	STD_LOGIC_VECTOR (9 downto 0):= CONV_STD_LOGIC_VECTOR(100, 10);
SIGNAL bar_Y_pos					:	STD_LOGIC_VECTOR (9 downto 0);

BEGIN

	barwidth <= CONV_STD_LOGIC_VECTOR(50,10);
	barheight <= CONV_STD_LOGIC_VECTOR(3,10);
	Bar_Y_pos <= CONV_STD_LOGIC_VECTOR(460,10);
	
	bar_width <= barwidth;
	bar_height <= barheight;
	
	Red <= bar_on;
	Blue <= '0';
	Green <= '0';
	
	RGB_Display: Process (Bar_X_pos, Bar_Y_pos, pixel_column, pixel_row, barwidth, barheight)
	BEGIN
			-- Set Bar_on ='1' to display ball
		IF ('0' & Bar_X_pos <= pixel_column + barwidth) AND
 			-- compare positive numbers only
		(Bar_X_pos + barwidth >= '0' & pixel_column) AND
		('0' & Bar_Y_pos <= pixel_row + barheight) AND
		(Bar_Y_pos + barheight >= '0' & pixel_row ) THEN
			Bar_on <= '1';
		ELSE
			Bar_on <= '0';
		END IF;
	END process RGB_Display;
	

	Move_Bar: process(vert_sync, key_in, pause)
	BEGIN
			-- Move bar once every vertical sync
		IF((vert_sync'event) and (vert_sync = '1')) AND pause = '0' then
	
			IF (key_in = "00011100" and new_key = '1') and bar_X_pos >= barwidth then
				bar_X_pos <= bar_X_pos - CONV_STD_LOGIC_VECTOR(5,10);
			ELSIF (key_in = "00100011" and new_key = '1') and bar_X_pos < CONV_STD_LOGIC_VECTOR(640,10) - barwidth then
				bar_X_pos <= bar_X_pos + CONV_STD_LOGIC_VECTOR(5,10);
			END IF;
		END if;
	
	END process Move_Bar;
	
	
END ARCHITECTURE;
	
	
	