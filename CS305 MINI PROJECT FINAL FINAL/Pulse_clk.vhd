LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

Entity Pulse_Clk is

	port(input_clk: IN STD_LOGIC;
		
	            Q : OUT STD_LOGIC); 
		 
end Entity;

ARCHITECTURE Behaviour of Pulse_Clk  is

SIGNAL tmp: STD_LOGIC_VECTOR(24 DOWNTO 0); 
 
	Begin
    Process(input_clk)
		begin
				if (rising_edge(input_clk)) then
			  
					if (tmp = CONV_STD_LOGIC_VECTOR(858938,25)) then
			  
						tmp <= CONV_STD_LOGIC_VECTOR(0,25);
						Q <= '1';
					
					else
			  
			        tmp <= tmp + 1;
					  Q <= '0';
					  
					end if;
				end if;
		
		end process;
 end Behaviour;
