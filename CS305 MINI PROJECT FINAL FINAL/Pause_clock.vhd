LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

Entity Pause_clk is

	port(input_clk : IN STD_LOGIC;
				    Q : OUT STD_LOGIC); 
		 
end Entity;

ARCHITECTURE Behaviour of Pause_clk is

SIGNAL tmp: STD_LOGIC_VECTOR(24 DOWNTO 0); 
 
	Begin
    Process(input_clk)
		begin
		
				if (rising_edge(input_clk)) then
			  
					if (tmp = "1011111010111100001000000") then
			  
						tmp <= "0000000000000000000000000";
						Q <= '1';
					
					else
			  
			        tmp <= tmp + 1;
					  Q <= '0';
					  
					end if;
				end if;
		end process;
 end Behaviour;
