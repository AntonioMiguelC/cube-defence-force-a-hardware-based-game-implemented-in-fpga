library IEEE;
use  IEEE.STD_LOGIC_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

entity clk_div is

Port (clk	:	in std_logic;
		clk_half	:	out std_logic
		);
end entity;


architecture arch of clk_div is

signal count	:	std_logic;

begin

	process(clk)
	begin
	
		if(rising_edge(clk)) then
			count <= not count;
		end if;
	
	end process;
	
	clk_half <= count;

end architecture arch;