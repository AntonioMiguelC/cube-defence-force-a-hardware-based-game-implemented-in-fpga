LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;
LIBRARY lpm;
USE lpm.lpm_components.ALL;

ENTITY ball IS
Generic(ADDR_WIDTH: integer := 12; DATA_WIDTH: integer := 1);

PORT(	  SIGNAL pause, PB2							: IN std_logic;
		  SIGNAL Col_detected						: IN std_logic;
		  SIGNAL pixel_row, pixel_column			: IN std_logic_vector (9 downto 0);
        SIGNAL Vert_sync_in						: IN std_logic;
		  SIGNAL increase_speed						: IN std_logic;
		  SIGNAL enable_game							: IN std_logic;
		  SIGNAL reset_speed							: IN std_logic;
		  SIGNAL rand_in								: IN std_logic_vector(3 downto 0);
		  SIGNAL Y_pos, X_pos, ball_size			: OUT std_logic_vector(9 DOWNTO 0);
		  SIGNAL Red,Green,Blue, ball_lost		: OUT std_logic;
		  SIGNAL rand_e								: OUT std_logic
		  );
END ball;


Architecture arch of ball is

SIGNAL Ball_on, Direction					: std_logic;
SIGNAL Size 									: std_logic_vector(9 DOWNTO 0);  
SIGNAL Ball_Y_motion, Ball_X_motion 	: std_logic_vector(9 DOWNTO 0);
SIGNAL Ball_Y_pos, Ball_X_pos				: std_logic_vector(9 DOWNTO 0);

begin

Size <= CONV_STD_LOGIC_VECTOR(8,10);
Y_pos <= Ball_Y_pos;
X_pos <= Ball_x_pos;
ball_size <= Size;

RGB_Display: Process (Ball_X_pos, Ball_Y_pos, pixel_column, pixel_row, Size)
BEGIN
			-- Set Ball_on ='1' to display ball
 IF ('0' & Ball_X_pos <= pixel_column + Size) AND
 			-- compare positive numbers only
 	(Ball_X_pos + Size >= '0' & pixel_column) AND
 	('0' & Ball_Y_pos <= pixel_row + Size) AND
 	(Ball_Y_pos + Size >= '0' & pixel_row ) THEN
 		Ball_on <= '1';
 	ELSE
 		Ball_on <= '0';
 END IF;
 END process RGB_Display;

Move_Ball: process(vert_sync_in, Ball_X_pos, Ball_y_pos, col_detected, increase_speed, pause,reset_speed)
variable temp_speed	: STD_LOGIC_VECTOR (9 downto 0);
variable y_speed		: STD_LOGIC_VECTOR (9 downto 0):=CONV_STD_LOGIC_VECTOR(5,10);
variable x_speed		: STD_LOGIC_VECTOR (9 downto 0):=CONV_STD_LOGIC_VECTOR(2,10);
BEGIN
	
	IF (reset_speed = '0') then
		IF(increase_speed'event and increase_speed = '1')then
			if x_speed <= CONV_STD_LOGIC_VECTOR(15,10) then
				x_speed := x_speed + CONV_STD_LOGIC_VECTOR(2,10);
			end if;
			if y_speed <= CONV_STD_LOGIC_VECTOR(80,10) then
				y_speed := y_speed + CONV_STD_LOGIC_VECTOR(5,10);
			end if;
		END IF;
	ELSE
		y_speed := CONV_STD_LOGIC_VECTOR(5,10);
		x_speed := CONV_STD_LOGIC_VECTOR(5,10);
	END IF;

	IF ((vert_sync_in'event and vert_sync_in = '1') AND pause = '0') AND (enable_game = '1') THEN
			
			ball_lost <= '0';
			
			-- Bounce off top of screen
			IF Ball_Y_pos <= Size THEN
				Ball_Y_motion <= y_speed;
			END IF;
			
			IF ('0' & Ball_X_pos) >= CONV_STD_LOGIC_VECTOR(640,10) - Size THEN
				Ball_X_motion <= x_speed - x_speed - x_speed;
			ELSIF Ball_X_pos <= Size + Size + conv_std_logic_vector(3,10) OR (x_speed > CONV_STD_LOGIC_VECTOR(10,10) AND Ball_X_pos <= Size + Size) THEN
				Ball_X_motion <= x_speed;
			ELSE
				Ball_x_motion <= ball_x_motion;
			END IF;
			
			-- If player catches or misses the ball, make a new ball reappear
			IF (col_detected = '1') OR (('0' & Ball_Y_pos) >= CONV_STD_LOGIC_VECTOR(480,10) - Size) THEN
				rand_e <= '1';
				Ball_Y_pos <= CONV_STD_LOGIC_VECTOR(20,10);
				
				case rand_in is
					when "0000" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(20,10);
					when "0001" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(55,10);
					when "0010" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(90,10);
					when "0011" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(125,10);
					when "0100" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(160,10);
					when "0101" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(195,10);
					when "0110" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(230,10);
					when "0111" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(265,10);
					when "1000" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(300,10);
					when "1001" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(335,10);
					when "1010" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(370,10);
					when "1011" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(405,10);
					when "1100" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(440,10);
					when "1101" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(475,10);
					when "1110" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(510,10);
					when "1111" =>
						Ball_X_pos <= CONV_STD_LOGIC_VECTOR(545,10);
				end case;
				
				--If player misses the ball, assert ball_lost signal
				IF (('0' & Ball_Y_pos) >= CONV_STD_LOGIC_VECTOR(480,10) - Size) THEN
					ball_lost <= '1';
				Else
					ball_lost <= '0';
				END IF;

			ELSE
				rand_e <= '0';
				Ball_Y_pos <= Ball_Y_pos + Ball_Y_motion;
				
				IF Ball_X_pos <= Size + Size + conv_std_logic_vector(5,10) then
					Ball_X_pos <= Size + Size + conv_std_logic_vector(6,10);
				ELSE
					Ball_X_pos <= Ball_X_pos + Ball_X_motion;
				END IF;
				
			END IF;
	ELSIF ((vert_sync_in'event and vert_sync_in = '1') AND pause = '1') AND (enable_game = '1') THEN
		temp_speed :=  Ball_Y_motion;
		Ball_Y_pos <= Ball_Y_pos;
	END IF;
		
END process Move_Ball;
 
 Red <= ball_on;
 Blue <= ball_on;
 Green <= '0';

 
 End architecture arch;

