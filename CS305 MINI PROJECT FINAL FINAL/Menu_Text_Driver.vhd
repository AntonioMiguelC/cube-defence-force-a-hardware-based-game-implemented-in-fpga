	LIBRARY IEEE;
	USE  IEEE.STD_LOGIC_1164.all;
	USE  IEEE.STD_LOGIC_ARITH.all;
	USE  IEEE.STD_LOGIC_UNSIGNED.all;

	ENTITY Menu_Text_driver IS

	PORT (output							            : out std_logic_vector(5 downto 0);
		  Pulse_Clk											: in std_logic;
		  pixel_row 					               : in std_logic_vector(9 downto 0);
		  pixel_column					               : in std_logic_vector(9 downto 0);
		  mode_screen										: in std_logic;
		  win_screen										: in std_logic;
		  lose_screen										: in std_logic;
		  SEL_ROW											: out std_logic;
		  SEL_COL										   : out std_logic;
		  n_clk												: in std_logic
		  );
		  
	END Menu_Text_driver;

	ARCHITECTURE Behaviour OF Menu_Text_driver IS

	SIGNAL tmp_column    : INTEGER:= 31;
	SIGNAL tmp_row       : INTEGER:= 320;
	SIGNAL tmp_column_1  : INTEGER:= 0;
	SIGNAL tmp_row_1     : INTEGER:= 160;
	SIGNAL Pulse_Time    : INTEGER:= 0;
	SIGNAL tmp_row2      : INTEGER:= 255;
	
	

		BEGIN

			PROCESS(pixel_column, pixel_row, mode_screen, tmp_column, tmp_row, tmp_column_1, tmp_row_1, mode_screen)

					BEGIN
					
					IF Mode_screen = '1' then
					--------------------------------------------------------
					--MODE SELECT: SELECT GAME MODE
					--SW0(UP ARROW): GAME
					--SW0 (DOWN ARROW): TRAINING
					--LEFT MOUSE CLICK TO BEGIN
					--------------------------------------------------------
					--------------------------------------------------------
					-- MODE SELECT
					--------------------------------------------------------
						IF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(tmp_column ,10))   AND
						
							 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 31),10))) AND
			
							((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(tmp_row2  ,10))   AND
							
							 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "001101"; --Display the letter 'M'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 31),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 63),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "001111"; --Display the letter 'O'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
			
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 63),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 95),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "000100"; --Display the letter 'D'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 95),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 127),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 127),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 159),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "100000"; --Display Spaces
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 159),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 191),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "010011"; --Display the letter 'S'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 191),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 223),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 223),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 255),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "001100"; --Display the letter 'L'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 255),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 287),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 287),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 319),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '1';											
									SEL_COL <= '1';	
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 319),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 351),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 31),10))) THEN
		
									output  <= "010100"; --Display the letter 'T'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
					----------------------------------------------------------------------------------------
					--SW0
					----------------------------------------------------------------------------------------
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 48),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 64),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 64),10))) THEN
		
									output  <= "010011"; --Display the letter 'S'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
			
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 64),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 80),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 64),10))) THEN
		
									output  <= "010111"; --Display the letter 'W'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 80),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 64),10))) THEN
		
									output  <= "001111"; --Display the letter '0'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 64),10))) THEN
		
									output  <= "011110"; -- Display UP ARROW
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 64),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 64),10))) THEN
		
									output  <= "000001"; --Display the letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 64),10))) THEN
		
									output  <= "001110"; --Display the letter 'N'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "000100"; --Display the letter 'D'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "100000"; --Display space
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 +48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "010011"; --Display the letter 'S'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
			        ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "010111"; --Display the letter 'W'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "110001"; --Display the letter '1'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
				     
		           ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "011100"; --Display the Down Arrow
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "111111"; --Display colon
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 304),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 302),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "000111"; --Display the Letter 'G'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "000001"; --Display the Letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "001101"; --Display the Letter 'M'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 366),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 48),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 64),10))) THEN
		
									output  <= "000101"; --Display the Letter 'E'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					-------------------------------------------------------------------------------------------
					-- Training
					-------------------------------------------------------------------------------------------
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 48),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 64),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "010011"; --Display the letter 'S'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
			
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 64),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 80),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "010111"; --Display the letter 'W'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 80),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "001111"; --Display the letter '0'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "011100"; -- Display DOWN ARROW
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "000001"; --Display the letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "001110"; --Display the letter 'N'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 96),10))) THEN
		
									output  <= "000100"; --Display the letter 'D'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "100000"; --Display space
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "010011"; --Display the letter 'S'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
			        ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "010111"; --Display the letter 'W'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "110001"; --Display the letter '1'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
				     
		           ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "011110"; --Display the UP Arrow
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "111111"; --Display colon
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 304),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 302),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "010100"; --Display the Letter 'T'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "010010"; --Display the Letter 'R'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "000001"; --Display the Letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 366),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "001001"; --Display the Letter 'I'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 366),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 382),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 80),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 96),10))) THEN
		
									output  <= "001110"; --Display the Letter 'N'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					----------------------------------------------------------------------------------------
					--Left Click to START
					----------------------------------------------------------------------------------------
					  ELSIF  ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 80),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
								 
								 
		
									output  <= "001100"; --Display the letter 'L'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
			
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "000110"; --Display the letter 'F'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "010100"; -- Display the letter 'T'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "100000"; --Display spaces
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "001100"; --Display the letter 'L'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 144),10))) THEN
		
									output  <= "001001"; --Display the letter 'I'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "001011"; --Display the letter 'K'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
			        ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "010100"; --Display the letter 'T'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
				     
		           ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "001111"; ---Display the letter '0'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 304),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 304),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "010011"; --Display the letter 'S'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "010100"; --Display the letter 'T'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "000001"; --Display the Letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 368),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "010010"; --Display the Letter 'R'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 368),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 384),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 128),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 144),10))) THEN
		
									output  <= "010100"; --Display the Letter 'T'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					
					----------------------------------------------------------------------------------------
					--Right Click to START
					----------------------------------------------------------------------------------------
					
					 	ELSIF  ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 80),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
								 
								 	output  <= "010010"; --Display the letter 'R'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
			
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 96),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "001001"; --Display the letter 'I'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 112),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "000111"; --Display the letter 'G'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 128),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "001000"; -- Display the letter 'H'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 144),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "010100"; --Display the letter 'T'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 160),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "100000"; --Display SPACE
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 176),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 192),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "001100"; --Display the letter 'L'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 208),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2  + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2  + 176),10))) THEN
		
									output  <= "001001"; --Display the letter 'I'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 224),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 240),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "001011"; --Display the letter 'K'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
			        ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 256),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 272),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "010100"; --Display the letter 'T'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
				     
		           ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 288),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 304),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "001111"; ---Display the letter '0'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 304),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 320),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "010000"; --Display the letter 'P'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 336),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "000001"; --Display the letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 352),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 368),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "010101"; --Display the Letter 'U'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 368),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 384),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "010011"; --Display the Letter 'S'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 384),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 400),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row2 + 160),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row2 + 176),10))) THEN
		
									output  <= "000101"; --Display the Letter 'E'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
----------------------------------------------------------------------------------------------------------
--DISPLAY BORDER									
----------------------------------------------------------------------------------------------------------

                 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((0),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((15),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((0),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((479),10))) THEN
		
									output  <= "111010"; --Display line
									SEL_ROW <= '0';											
									SEL_COL <= '0';

				     ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((15),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((31),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((0),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((479),10))) THEN
		
									output  <= "111010"; -- --Display line
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((607),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((623),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((0),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((479),10))) THEN
		
									output  <= "111010"; -- --Display line
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					 
					 	ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((623),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((639),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((0),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((479),10))) THEN
		
									output  <= "111010"; -- --Display line
									SEL_ROW <= '0';											
									SEL_COL <= '0';
										
										
						ELSE
						
							output  <= "100000"; --Display SPACES
						
							
					END IF;
					ELSIF win_screen = '1' then
					--------------------------------------------------------
					--WIN :)
					--SCORE : 99
					--------------------------------------------------------
						IF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(191,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(223,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "011001"; --Display the letter 'Y'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
					  	ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "001111"; --Display the letter '0'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(255,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(287,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "010101"; --Display the letter 'U'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(287,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR (319,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(319,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(351,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "010111"; --Display the letter 'W'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(351,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(383,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "001001"; --Display the letter 'I'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
					    ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(383,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(415,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "001110"; --Display the letter 'N'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(415,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(447,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "100001"; --Display the letter '!'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
							ELSE
						
							output  <= "100000"; --Display SPACES
							SEL_ROW <= '1';											
							SEL_COL <= '1';
							
						END IF;
					
					ELSIF lose_screen = '1' then
					--------------------------------------------------------
					--LOSE :(
					--SCORE : 99
					--------------------------------------------------------	
							IF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(191,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(223,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "011001"; --Display the letter 'Y'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
					  	ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "001111"; --Display the letter '0'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(255,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(287,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "010101"; --Display the letter 'U'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(287,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR (319,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "100000"; --Display Space
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(319,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(351,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "001100"; --Display the letter 'L'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(351,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(383,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "001111"; --Display the letter 'O'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
					    ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(383,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(415,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "010011"; --Display the letter 'S'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(415,10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR(447,10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(223,10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR(255,10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
							ELSE
						
							output  <= "100000"; --Display SPACES
							SEL_ROW <= '1';											
							SEL_COL <= '1';
							
						END IF;
					
					ELSE

						--------------------------------------------------------
						--CUBE DEFENCE FORCE
						--------------------------------------------------------
						IF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR(tmp_column ,10))   AND
						
							 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 31),10))) AND
			
							((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR(tmp_row ,10))   AND
							
							 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 31),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 63),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "010101"; --Display the letter 'U'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
			
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 63),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 95),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000010"; --Display the letter 'B'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 95),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 127),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 127),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 159),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "100000"; --Display Spaces
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 159),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 191),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000100"; --Display the letter 'D'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 191),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 223),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 223),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 255),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000110"; --Display the letter 'F'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 255),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 287),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 287),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 319),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "001110"; --Display the letter 'N'
									SEL_ROW <= '1';											
									SEL_COL <= '1';	
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 319),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 351),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 351),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 383),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 383),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 415),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "100000"; --Display Spaces
									SEL_ROW <= '1';											
									SEL_COL <= '1';
									 
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 415),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 447),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000110"; --Display the letter 'F'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
								
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 447),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 479),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "001111"; --Display the letter 'O'
									SEL_ROW <= '1';											
									SEL_COL <= '1';
								
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 479),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 511),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "010010"; --Display the letter 'R'
									SEL_ROW <= '1';											
									SEL_COL <= '1';	
											
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 511),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 543),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '1';											
									SEL_COL <= '1';	
										
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column + 543),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column + 575),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '1';											
									SEL_COL <= '1';	
									
	------------------------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------------------------									
									
					ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 63),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 79),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
									
									output  <= "000010"; --Display the letter 'B'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 79),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 95),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "001100"; --Display the letter 'L'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 95),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 111),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "001111"; --Display the letter 'O'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 111),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 127),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "000011"; --Display the letter 'C'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 127),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 143),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "001011"; --Display the letter 'K'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 143),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 159),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "100000"; --Display Spaces
									SEL_ROW <= '0';											
									SEL_COL <= '0';
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 159),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 175),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "000001"; --Display the letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 175),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 191),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "010010"; --Display the letter 'R'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 191),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 207),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "001101"; --Display the letter 'M'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 207),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 223),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "000001"; --Display the letter 'A'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 223),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 239),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "000111"; --Display the letter 'G'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									 
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 239),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 255),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "000101"; --Display the letter 'E'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
								
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 255),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 271),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "000100"; --Display the letter 'D'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
								
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 271),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 287),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "000100"; --Display the letter 'D'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
											
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 287),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 303),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "001111"; --Display the letter 'O'
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
										
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 303),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 319),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row + 31),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row + 47),10))) THEN
		
									output  <= "001110"; --Display the letter 'N'
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
	------------------------------------------------------------------------------------------------------
	--PULSE
	------------------------------------------------------------------------------------------------------	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 31),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 47),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
								  SEL_ROW <= '0';											
								  SEL_COL <= '0';
									
								  case Pulse_Time is

								  when 0 =>  output <= "111101";
								  when 1 =>  output <= "111101";
								  when 2 =>  output <= "111101";
								  when 3 =>  output <= "111101";
							

								  when others => output <= "100000";

								  END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 47),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 63),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									
									when 1 =>  output <= "111101";
									when 2 =>  output <= "111101";
									when 3 =>  output <= "111101";
									when 4 =>  output <= "111101";
							

									when others => output <= "100000";

									END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 63),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 79),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
								
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

								
									when 2 =>  output <= "111101";
									when 3 =>  output <= "111101";
									when 4 =>  output <= "111101";
									when 5 =>  output <= "111101";
								

									when others => output <= "100000";

									END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 79),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 95),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
																	
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

								
									when 3 =>  output <= "111101";
									when 4 =>  output <= "111101";
									when 5 =>  output <= "111101";
									when 6 =>  output <= "111101";
								

									when others => output <= "100000";

									END CASE;
																	
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 95),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 111),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
								 
								 SEL_ROW <= '0';											
								 SEL_COL <= '0';
		
									case Pulse_Time is

								
									when 4 =>  output <= "111101";
									when 5 =>  output <= "111101";
									when 6 =>  output <= "111101";
									when 7 =>  output <= "111101";
							

									when others => output <= "100000";

									END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 111),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 127),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

								
									when 5 =>  output <= "111101";
									when 6 =>  output <= "111101";
									when 7 =>  output <= "111101";
									when 8 =>  output <= "111101";
																	

									when others => output <= "100000";

									END CASE;
						
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 127),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 143),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							 
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 187),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									
									when 6 =>  output <= "111101";
									when 7 =>  output <= "111101";
									when 8 =>  output <= "111101";
									when 9 =>  output <= "111101";
								

									when others => output <= "100000";

									END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 143),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 159),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 125),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 203),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

								
									when 7 =>  output <= "111101";
									when 8 =>  output <= "111101";
									when 9 =>  output <= "111101";
									when 10 =>  output <= "111101";
									
								

									when others => output <= "100000";

									END CASE;
					
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 159),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 175),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 93),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 235),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

							
									when 8  =>  output <= "111101";
									when 9  =>  output <= "111101";
									when 10 =>  output <= "111101";
									when 11 =>  output <= "111101";
								

									when others => output <= "100000";

									END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 175),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 191),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 45),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 251),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
									case Pulse_Time is
							
									when 9  =>  output <= "111101";
									when 10 =>  output <= "111101";
									when 11 =>  output <= "111101";
									when 12 =>  output <= "111101";
							
									when others => output <= "100000";

									END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 191),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 207),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 77),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 235),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is
							
									when 10 => output <= "111101";
									when 11 => output <= "111101";
									when 12 => output <= "111101";
									when 13 => output <= "111101";
							
									when others => output <= "100000";

									END CASE;

						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 207),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 223),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 109),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 219),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is
								
									when 11 => output <= "111101";
									when 12 => output <= "111101";
									when 13 => output <= "111101";
									when 14 => output <= "111101";
							
									when others => output <= "100000";

									END CASE;

						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 223),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 239),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 125),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 203),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is
							
									when 12 => output <= "111101";
									when 13 => output <= "111101";
									when 14 => output <= "111101";
									when 15 => output <= "111101";
							
									when others => output <= "100000";

									END CASE;
							
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 239),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 255),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is
								
									when 13 => output <= "111101";
									when 14 => output <= "111101";
									when 15 => output <= "111101";
									when 16 => output <= "111101";
							
									when others => output <= "100000";

									END CASE;
								
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 255),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 271),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 14 => output <= "111101";
									when 15 => output <= "111101";
									when 16 => output <= "111101";
									when 17 => output <= "111101";
								
									when others => output <= "100000";

									END CASE;
								
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 271),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 287),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
									case Pulse_Time is

									when 15 => output <= "111101";
									when 16 => output <= "111101";
									when 17 => output <= "111101";
									when 18 => output <= "111101";
								
									when others => output <= "100000";

									END CASE;
											
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 287),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 303),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
									
									case Pulse_Time is

									when 16 => output <= "111101";
									when 17 => output <= "111101";
									when 18 => output <= "111101";
									when 19 => output <= "111101";
								
									when others => output <= "100000";

									END CASE;
										
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 303),10))   AND --Second Pulse
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 319),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 17 => output <= "111101";
									when 18 => output <= "111101";
									when 19 => output <= "111101";
									when 20 => output <= "111101";
								
									when others => output <= "100000";

									END CASE;
						
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 319),10))   AND 
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 335),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 127),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';	
														
									case Pulse_Time is

									when 18 => output <= "111101";
									when 19 => output <= "111101";
									when 20 => output <= "111101";
									when 21 => output <= "111101";
							
									when others => output <= "100000";

									END CASE;
						
					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 335),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 351),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 93),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 203),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';  
									
									
									case Pulse_Time is

									when 19 => output <= "111101";
									when 20 => output <= "111101";
									when 21 => output <= "111101";
									when 22 => output <= "111101";
							
									when others => output <= "100000";

									END CASE;
					
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 351),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 367),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 61),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 219),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 20 => output <= "111101";
									when 21 => output <= "111101";
									when 22 => output <= "111101";
									when 23 => output <= "111101";
							
									when others => output <= "100000";

									END CASE;
									
					 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 367),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 383),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 109),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 203),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 21 => output <= "111101";
									when 22 => output <= "111101";
									when 23 => output <= "111101";
									when 24 => output <= "111101";
									
									when others => output <= "100000";

									END CASE;

					  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 383),10))   AND 
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 399),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 125),10))   AND  
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 22 => output <= "111101";
									when 23 => output <= "111101";
									when 24 => output <= "111101";
									when 25 => output <= "111101";

									when others => output <= "100000";

									END CASE;
									
						ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 399),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 415),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 125),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 23 => output <= "111101";
									when 24 => output <= "111101";
									when 25 => output <= "111101";
									when 26 => output <= "111101";

									when others => output <= "100000";

									END CASE;
										
						 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 415),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 431),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 125),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 24 => output <= "111101";
									when 25 => output <= "111101";
									when 26 => output <= "111101";
									when 27 => output <= "111101";

									when others => output <= "100000";

									END CASE;
															
						  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 431),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 447),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 93),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 25 => output <= "111101";
									when 26 => output <= "111101";
									when 27 => output <= "111101";
									when 28 => output <= "111101";

								   when others => output <= "100000";

									END CASE;
									
						  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 447),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 463),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 61),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 203),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 26 => output <= "111101";
									when 27 => output <= "111101";
									when 28 => output <= "111101";
									when 29 => output <= "111101";

									when others => output <= "100000";

									END CASE;
							
							ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 463),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 479),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 13),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 219),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 27 => output <= "111101";
									when 28 => output <= "111101";
									when 29 => output <= "111101";
									when 30 => output <= "111101";
						
									when others => output <= "100000";

									END CASE;

							 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 479),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 495),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 - 51),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 251),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 28 => output <= "111101";
									when 29 => output <= "111101";
									when 30 => output <= "111101";
									when 31 => output <= "111101";

									when others => output <= "100000";

									END CASE;
								
							  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 495),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 511),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 - 19),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 235),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 29 => output <= "111101";
									when 30 => output <= "111101";
									when 31 => output <= "111101";
									when 32 => output <= "111101";

									when others => output <= "100000";

									END CASE;
															
							  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 511),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 527),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 29),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 219),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 30 => output <= "111101";
									when 31 => output <= "111101";
									when 32 => output <= "111101";
									when 33 => output <= "111101";

									when others => output <= "100000";

									END CASE;
								
							  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 527),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 543),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 61),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 203),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 31 => output <= "111101";
									when 32 => output <= "111101";
									when 33 => output <= "111101";
									when 34 => output <= "111101";

									when others => output <= "100000";

									END CASE;
								
								ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 543),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 559),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 93),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 32 => output <= "111101";
									when 33 => output <= "111101";
									when 34 => output <= "111101";
									when 35 => output <= "111101";

									when others => output <= "100000";

									END CASE;
									
								 ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 559),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 575),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 125),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 33 => output <= "111101";
									when 34 => output <= "111101";
									when 35 => output <= "111101";
									when 36 => output <= "111101";

									when others => output <= "100000";

									END CASE;
								 
								  ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 575),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 591),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 34 => output <= "111101";

									when others => output <= "100000";

									END CASE;
									
									ELSIF ((pixel_column(9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 591),10))   AND
						
								 (pixel_column(9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_column_1 + 607),10))) AND
			
								((pixel_row  (9 downto 0) >= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 141),10))   AND
							
								 (pixel_row  (9 downto 0) <= CONV_STD_LOGIC_VECTOR((tmp_row_1 + 171),10))) THEN
		
									SEL_ROW <= '0';											
									SEL_COL <= '0';
									
									case Pulse_Time is

									when 35 => output <= "111101";

									when others => output <= "100000";

									END CASE;
															
						ELSE	  
									output <= "100000"; --Display the spaces
									SEL_ROW <= '0';											
									SEL_COL <= '0';
						
						END IF;
					END IF;
				
			END PROCESS;
	------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------
			
	Pause_Count: Process(Pulse_Clk)

		Begin
				if (Pulse_Clk = '1') then
				  
						if (Pulse_Time = 36) then
				  
							 Pulse_Time<= 0; 
			
						else
						
							 Pulse_Time <= Pulse_Time + 1;
											  
					end if;
				end if;
	end process;

			
	END ARCHITECTURE;
						
		
		