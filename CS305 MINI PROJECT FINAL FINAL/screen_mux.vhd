LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

Entity Screen_MUX is

port ( game_r, game_g, game_b		:	IN std_logic;
		 lose_r,lose_g,lose_b		: IN std_logic;
		 sel								: IN std_logic;
		 out_r,out_g,out_b			: OUT std_logic);
		 
END ENTITY;

Architecture arch of screen_mux is

Begin

	process(sel)
	
	begin
		case sel is
		
		when '1' =>
			out_r <= game_r;
			out_g <= game_g;
			out_b <= game_b;
			
		when '0' =>
			out_r <=	lose_r;
			out_g <= lose_g;
			out_b <= lose_b;
		end case;
		
	end process;

end arch;
		