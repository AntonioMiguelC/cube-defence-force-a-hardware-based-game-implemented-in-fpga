library IEEE;
use  IEEE.STD_LOGIC_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

Entity whitescreen is

Port (red, green, blue	:	out std_logic);

end whitescreen;

architecture arch of whitescreen is

begin

red <= '0';
blue <= '0';
green <= '0';

end arch;