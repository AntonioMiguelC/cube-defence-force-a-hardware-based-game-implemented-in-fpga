LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

Entity mux_six is

Port (	in0	: in std_logic_vector(5 downto 0);
			in1	: in std_logic_vector(5 downto 0);
			sel	: in std_logic;
			
			out1	: out std_logic_vector(5 downto 0));
			
End entity;

Architecture arch of mux_six is

Begin

	process(sel)
	
	Begin
	
		if (sel = '0') then
			
			out1 <= in0;
		else
			out1 <= in1;
		end if;
		
	end process;
	
end arch;