library IEEE;
use  IEEE.STD_LOGIC_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

Entity Col_detector is

Port (Bar_x, bar_y, ball_x, ball_y	: in std_logic_vector(9 downto 0);
		ball_size, barwidth, barheight: in std_logic_vector (9 downto 0);
		col_det								: out std_logic);
		
End col_detector;

Architecture arch of col_detector is

Begin

Process(Bar_x, bar_y, ball_x, ball_y,ball_size, barwidth, barheight)

begin
	IF (((Ball_x + ball_Size >= Bar_x - BARWIDTH) AND (Ball_x + ball_Size <= Bar_x + BARWIDTH + BARWIDTH)) AND ((Ball_y + ball_size >= Bar_y))) THEN

			col_det <= '1';
	ELSE
			col_det <= '0';
	END IF;
END PROCESS;

end arch;

