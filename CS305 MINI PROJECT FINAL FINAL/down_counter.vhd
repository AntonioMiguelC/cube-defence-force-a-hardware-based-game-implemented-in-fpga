LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

Entity Down_counter is

	port(vert_sync_out: IN STD_LOGIC;
	     Q : OUT STD_LOGIC); 
		 
end Entity;

ARCHITECTURE Behaviour of Down_counter is

SIGNAL tmp: STD_LOGIC_VECTOR(4 DOWNTO 0); 

	Begin
		Counter: Process(vert_sync_out)
		begin
			if (vert_sync_out = '1') then
			  
			  if (tmp = "00000") then
			  
			      tmp <= "01001";
					  Q <= '1';
					
			  else
			  
			        tmp <= tmp - "00001";
					  Q <= '0';
					  
				end if;
			end if;
		end process;
 end Behaviour;

