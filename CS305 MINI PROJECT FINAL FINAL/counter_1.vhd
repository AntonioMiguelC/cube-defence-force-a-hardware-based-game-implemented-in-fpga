LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

Entity Counter_1 is

	port(vert_sync_out: IN STD_LOGIC;
	     Q : OUT STD_LOGIC); 
		 
end Entity;

ARCHITECTURE Behaviour of Counter_1 is

SIGNAL tmp: STD_LOGIC_VECTOR(4 DOWNTO 0); 

	Begin
		Counter: Process(vert_sync_out)
		begin
			if (vert_sync_out = '1') then
			  
			  if (tmp = "11110") then
			  
			      tmp <= "00000";
					  Q <= '1';
					
			  else
			  
			        tmp <= tmp + 1;
					  Q <= '0';
					  
				end if;
			end if;
		end process;
 end Behaviour;


			 
			  
			    