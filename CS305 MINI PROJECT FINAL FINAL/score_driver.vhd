LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

Entity Score_driver is

port(output							               : out std_logic_vector(5 downto 0);
	  lose_game						               : out std_logic;
	  up_ball_speed									: out STD_LOGIC;
     counter_in, Down_counter, update_life 	: in std_logic;
     pixel_row 					               : in std_logic_vector(9 downto 0);
	  pixel_column					               : in std_logic_vector(9 downto 0);
	  pause											   : in std_logic;
	  count_zero										: out std_logic
	  
	  );
	  
End score_driver;

Architecture Behaviour of score_driver is
SIGNAL temp_ONES: STD_LOGIC_VECTOR(3 DOWNTO 0); 
SIGNAL temp_tens: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL lives	 :	integer:= 9;
SIGNAL TMP_ONES : STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";
SIGNAL TMP_TENS : STD_LOGIC_VECTOR(3 DOWNTO 0):="1001";


Begin

Text_Display: Process(pause, pixel_column, pixel_row,temp_ones, temp_tens, lives)

BEGIN

--------------------------------------------------------
--DISPLAY SCORE
--------------------------------------------------------


  IF (pixel_column(9 downto 4) = "011111") AND
 	 	
 	  (pixel_row(9 downto 4)  = "000001") then
 	
 		output <= "010011"; --Display the letter 'S'
		
	ELSIF (pixel_column(9 downto 4) = "100000") AND
 			 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "000011"; --Display the letter 'C'
			 
	ELSIF (pixel_column(9 downto 4) = "100001") AND
 			 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "001111"; --Display the letter 'O'
			 
	ELSIF (pixel_column(9 downto 4) = "100010") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "010010"; --Display the letter 'R'
			 
	ELSIF (pixel_column(9 downto 4) = "100011") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "000101"; --Display the letter 'E'
		 
	ELSIF (pixel_column(9 downto 4) = "100100") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    output <= "111111"; --Display Colon
			 
	ELSIF (pixel_column(9 downto 4) = "100101") AND
 			-- compare positive numbers only
 	
 	      (pixel_row(9 downto 4)  = "000001") then
 	
 		    case temp_tens is

			when "0000" => output <= "110000" ;
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;
			 	
	ELSIF (pixel_column(9 downto 4) = "100110") AND
	
 		   (pixel_row   (9 downto 4) = "000001") then
		
	case temp_ones is

	when "0000" => output <= "110000" ;
	when "0001" => output <= "110001";
	when "0010" => output <= "110010";
	when "0011" => output <= "110011";
	when "0100" => output <= "110100";
	when "0101" => output <= "110101";
	when "0110" => output <= "110110";
	when "0111" => output <= "110111";
	when "1000" => output <= "111000";
	when "1001" => output <= "111001";
	when others => output <= "100000";
	END CASE;
	
----------------------------------------------------------
--DISPLAY LIVES
----------------------------------------------------------	
	
	ELSIF (pixel_column(9 downto 4) = "011111") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "001100"; --Display 'L'
			 
	ELSIF (pixel_column(9 downto 4) = "100000") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "001001"; --Display 'I'
			 
	ELSIF (pixel_column(9 downto 4) = "100001") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "010110"; --Display 'V'
			 
	ELSIF (pixel_column(9 downto 4) = "100010") AND
	
 		   (pixel_row   (9 downto 4) = "000010") then
			
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "100011") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "010011"; --Display 'S'
			 
	ELSIF (pixel_column(9 downto 4) = "100100") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "111111"; --Display Colon	

  ELSIF (pixel_column(9 downto 4) = "100101") AND
	
 		  (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "110000"; --Display '0'	

 ELSIF (pixel_column(9 downto 4) = "100110") AND
	
 		  (pixel_row(9 downto 4)  = "000010") then
			
			 case lives is

	when 0 => output <= "110000" ;
	when 1 => output <= "110001";
	when 2 => output <= "110010";
	when 3 => output <= "110011";
	when 4 => output <= "110100";
	when 5 => output <= "110101";
	when 6 => output <= "110110";
	when 7 => output <= "110111";
	when 8 => output <= "111000";
	when 9 => output <= "111001";
	when others => output <= "100000";
	END CASE;	
			 
--------------------------------------------------------------
--DISPLAY LEVEL
--------------------------------------------------------------
			 
	ELSIF (pixel_column(9 downto 4) = "000001") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "001100"; --Display 'L'
			 
	ELSIF (pixel_column(9 downto 4) = "000010") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "000011") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "010110"; --Display 'V'
			 
	ELSIF (pixel_column(9 downto 4) = "000100") AND
	
 		   (pixel_row   (9 downto 4) = "000001") then
			
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "000101") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "001100"; --Display 'L'
			 
	ELSIF (pixel_column(9 downto 4) = "000110") AND
	
 		   (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "111111"; --Display Colon	

  ELSIF (pixel_column(9 downto 4) = "000111") AND
	
 		  (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "110000"; --Display '0'	

 ELSIF (pixel_column(9 downto 4) = "001000") AND
	
 		  (pixel_row(9 downto 4)  = "000001") then
			
			 output <= "110001"; --Display '1'				 

-------------------------------------------------------
--DISPLAY TIMER
-------------------------------------------------------
	ELSIF (pixel_column(9 downto 4) = "000001") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "010100"; --Display 'T'
			 
	ELSIF (pixel_column(9 downto 4) = "000010") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "001001"; --Display 'I'
			 
	ELSIF (pixel_column(9 downto 4) = "000011") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "001101"; --Display 'M'
			 
	ELSIF (pixel_column(9 downto 4) = "000100") AND
	
 		   (pixel_row   (9 downto 4) = "000010") then
			
			 output <= "000101"; --Display 'E'
			 
	ELSIF (pixel_column(9 downto 4) = "000101") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "010010"; --Display 'R'
			 
	ELSIF (pixel_column(9 downto 4) = "000110") AND
	
 		   (pixel_row(9 downto 4)  = "000010") then
			
			 output <= "111111"; --Display Colon	

  ELSIF (pixel_column(9 downto 4) = "000111") AND
	
 		  (pixel_row(9 downto 4)  = "000010") then
			
		  	  
 		    case TMP_TENS is

			when "0000" => output <= "110000";
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;	

 ELSIF (pixel_column(9 downto 4) = "001000") AND
	
 		 (pixel_row(9 downto 4)  = "000010") then
			
			 
 		    case TMP_ONES is

			when "0000" => output <= "110000" ;
			when "0001" => output <= "110001";
			when "0010" => output <= "110010";
			when "0011" => output <= "110011";
			when "0100" => output <= "110100";
			when "0101" => output <= "110101";
			when "0110" => output <= "110110";
			when "0111" => output <= "110111";
			when "1000" => output <= "111000";
			when "1001" => output <= "111001";
			when others => output <= "100000";
			END CASE;
--------------------------------------------------
--DISPLAY PAUSE			
-------------------------------------------------
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(17,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
			
				 output <= "010000"; --Display 'P'
			 
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(18,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
			
				output <= "000001"; --Display 'A'
			 
	   ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(19,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
			
				output <= "010101"; --Display 'U'
			 
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(20,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
			
				output <= "010011"; --Display 'S'
			 
	   ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(21,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
			
				output <= "000101"; --Display 'E'
			 
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(22,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
			
				output <= "000100"; --Display 'D'
				
		ELSIF (pixel_column(9 downto 4) = CONV_STD_LOGIC_VECTOR(23,6)) AND
	
				(pixel_row(9 downto 4)  = CONV_STD_LOGIC_VECTOR(15,6)) AND
			
				(pause = '1') THEN
			
				output <= "100001"; --Display 'D'		
	
		ELSE
				output <= "100000"; --Display Spaces
	END IF;	 

END process Text_Display;

------------------------------------------------------
------------------------------------------------------

--CONV_STD_LOGIC_VECTOR(640,10)

Score_Count: Process(counter_in)

	Begin
			if (counter_in = '1') then
			  
			  if (temp_ones = "1001") and (temp_tens /= "1001") then
			  
			      temp_ones <= "0000";
					temp_tens <= temp_tens + 1;
					up_ball_speed <= '1';
					
			  elsif (temp_ones = "1001") and (temp_tens = "1001") then
					temp_ones <= "0000";
					temp_tens <= "0000";
					up_ball_speed <= '1';
			  else
			        temp_ones <= temp_ones + 1;
					  up_ball_speed <= '0';
					  
				end if;
			end if;
		end process;
		
LIVES_COUNT: Process(update_life)

begin

if (update_life = '1') then
	if(lives = 0) then
		lose_game <= '1';
	else 
		lives <= lives - 1;
		lose_game <= '0';
	end if;
end if;
end process;

Time_Count: Process(Down_counter)

	Begin
			if (Down_counter = '1') then
			  
			  if (TMP_ONES = "0000") and (TMP_TENS /= "0000") then
			  
			      TMP_ONES <= "1001";
					TMP_TENS <= TMP_TENS - "0001";
					count_zero <= '0';
					
			  elsif (TMP_ONES = "0000") and (TMP_TENS = "0000") then
					count_zero <= '1';
			  else
			        TMP_ONES <= TMP_ONES - 1;
					  count_zero <= '0';
					  
				end if;
			end if;
		end process;

End Behaviour;
