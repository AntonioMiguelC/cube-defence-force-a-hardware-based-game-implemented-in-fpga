library IEEE;
use  IEEE.STD_LOGIC_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

Entity global_pause is

Port ( 	PB1	:	IN STD_LOGIC;
			pause	:	OUT STD_LOGIC);
			
END global_pause;

Architecture arch of global_pause is

Begin

	process(pb1)
	variable setPause	:	std_logic;

	begin
		
		if(pb1 = '1') then
			if setPause = '1' then
				setPause := '0';
			else
				setpause := '1';
			end if;
		end if;
		pause <= setPause;
		
	end Process;
	
End arch;