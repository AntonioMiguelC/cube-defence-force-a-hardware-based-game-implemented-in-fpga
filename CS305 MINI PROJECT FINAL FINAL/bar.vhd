LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;
LIBRARY lpm;
USE lpm.lpm_components.ALL;

ENTITY bar IS
Generic(ADDR_WIDTH: integer := 12; DATA_WIDTH: integer := 1);

PORT(	  SIGNAL Red,Green,Blue 								: OUT std_logic;
		  SIGNAL X_Pos, Y_Pos, bar_height, bar_width		: OUT std_logic_vector(9 downto 0);
		  SIGNAL mouse_in											: IN std_logic_vector (9 downto 0);
		  SIGNAL pixel_row, pixel_column						: IN std_logic_vector (9 downto 0);
        SIGNAL Vert_sync, pause, bar_shrink,reset, train				: IN std_logic);		
END bar;

ARCHITECTURE arch of bar is

SIGNAL bar_on						:	STD_LOGIC;
SIGNAL barheight 					:	STD_LOGIC_VECTOR (9 downto 0);
SIGNAL barwidth					:	STD_LOGIC_VECTOR (9 downto 0):= CONV_STD_LOGIC_VECTOR(50,10);
SIGNAL bar_X_pos, bar_Y_pos	:	STD_LOGIC_VECTOR (9 downto 0);

BEGIN

	barheight <= CONV_STD_LOGIC_VECTOR(3,10);
	Bar_Y_pos <= CONV_STD_LOGIC_VECTOR(460,10);
	
	Y_Pos <= Bar_Y_pos;
	X_Pos <= Bar_X_pos;
	bar_width <= barwidth;
	bar_height <= barheight;
	
	Red <= '0';
	Blue <= '0';
	Green <= bar_on;
	
	process (bar_shrink, reset)
	begin
		IF(rising_edge(bar_shrink)) and (barwidth > CONV_STD_LOGIC_VECTOR(5,10)) then
			barwidth <= barwidth - CONV_STD_LOGIC_VECTOR(5,10);
		end if;
		
		IF reset = '1' or train = '1' then
			barwidth <= CONV_STD_LOGIC_VECTOR(50,10);
		END IF;
	end process;
	
	RGB_Display: Process (Bar_X_pos, Bar_Y_pos, pixel_column, pixel_row, barwidth, barheight)
	BEGIN
	
	
			-- Set Bar_on ='1' to display ball
		IF ('0' & Bar_X_pos <= pixel_column + barwidth) AND
 			-- compare positive numbers only
		(Bar_X_pos + barwidth >= '0' & pixel_column) AND
		('0' & Bar_Y_pos <= pixel_row + barheight) AND
		(Bar_Y_pos + barheight >= '0' & pixel_row ) THEN
			Bar_on <= '1';
		ELSE
			Bar_on <= '0';
		END IF;
	END process RGB_Display;
	

	Move_Bar: process(vert_sync, mouse_in, pause)
	BEGIN
			-- Move bar once every vertical sync
		IF((vert_sync'event) and (vert_sync = '1')) AND pause = '0' then
	
			IF ('0' & mouse_in) < CONV_STD_LOGIC_VECTOR(640,10) - BARWIDTH - BARWIDTH THEN
				Bar_X_pos <= mouse_in + BARWIDTH;
			ELSE
				Bar_X_pos <= CONV_STD_LOGIC_VECTOR(640,10) - BARWIDTH;
			END IF;
		ELSIF ((vert_sync'event) and (vert_sync = '1')) AND pause = '1' then
			Bar_X_pos <= Bar_X_pos;		
		END IF;
	
	END process Move_Bar;
	
	
END ARCHITECTURE;
	
	
	