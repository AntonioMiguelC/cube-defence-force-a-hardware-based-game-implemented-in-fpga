LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

Entity Timer_clk is

	port(input_clk,pause, enable_count: IN STD_LOGIC;
				  --PB1: IN STD_LOGIC;
	            Q : OUT STD_LOGIC); 
		 
end Entity;

ARCHITECTURE Behaviour of Timer_clk is

SIGNAL tmp: STD_LOGIC_VECTOR(24 DOWNTO 0); 
 
	Begin
    Process(input_clk, pause, enable_count)
		begin
		
			if pause = '0' AND enable_count = '1' then
				if (rising_edge(input_clk)) then
			  
					if (tmp = "1011111010111100001000000") then
			  
						tmp <= "0000000000000000000000000";
						Q <= '1';
					
					else
			  
			        tmp <= tmp + 1;
					  Q <= '0';
					  
					end if;
				end if;
			else 
				tmp <= tmp;
				q <= '0';
			end if;
		end process;
 end Behaviour;
